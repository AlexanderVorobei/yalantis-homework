.PHONY: all \
		setup \
		lint \
		run

venv/bin/activate: ## alias for virtual environment
	python3 -m venv venv

setup: venv/bin/activate ## project setup
	. venv/bin/activate; pip install pip wheel setuptools
	. venv/bin/activate; pip install -r requirements.txt

run: venv/bin/activate ## Run
	. venv/bin/activate; python3 main.py

test: venv/bin/activate ## run tests
	. venv/bin/activate; python3 -m pytest -s -vv

lint:
	. venv/bin/activate; flake8 --config=./.config/.flake ./
